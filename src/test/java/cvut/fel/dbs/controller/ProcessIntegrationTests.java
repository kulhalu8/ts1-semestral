package cvut.fel.dbs.controller;

import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.*;


class ProcessIntegrationTests {
    private EntityManager em;
    private BillController billController;
    private CustomerController customerController;

    @BeforeEach
    public void setup() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("dbs-test");;
        em = emf.createEntityManager();

        this.billController = new BillController(em);
        this.customerController = new CustomerController(em);

        em.getTransaction().begin();
    }

    @AfterEach
    public void end() {
        this.em.flush();
        this.em.getTransaction().rollback();
    }

    @Test
    public void process_newBill_firstBill() {
        Customer c = this.customerController.registerCustomer("Josef", "Novák");
        Bill bill = this.billController.newBill(c);

        assertEquals(0, bill.getItems().size());
    }

    @Test
    public void process_newBill_hasOpenBill() {
        Customer c = this.customerController.registerCustomer("Josef", "Novák");

        Bill bill1 = this.billController.newBill(c);
        Bill bill2 = this.billController.newBill(c);

        assertEquals(bill1, bill2);
    }

    @Test
    public void process_payBill_openBill() {
        Customer c = this.customerController.registerCustomer("Josef", "Novák");
        Bill bill = this.billController.newBill(c);

        boolean paymentState = this.billController.payBill(bill);
        assertEquals(true, paymentState);
    }

    @Test
    public void process_payBill_closedBill() {
        Customer c = this.customerController.registerCustomer("Josef", "Novák");
        Bill bill = this.billController.newBill(c);
        bill.setOpen(false);

        boolean paymentState = this.billController.payBill(bill);
        assertEquals(false, paymentState);
    }
}