package cvut.fel.dbs.controller;

import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillConsistsOf;
import cvut.fel.dbs.model.Customer;
import cvut.fel.dbs.model.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class UnitTests {
    private EntityManager em;
    private BillController billController;
    private CustomerController customerController;

    @BeforeEach
    public void setup() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("dbs-test");;
        em = emf.createEntityManager();

        this.billController = new BillController(em);
        this.customerController = new CustomerController(em);
        em.getTransaction().begin();
    }

    @AfterEach
    public void end() {
        //this.em.flush();
        this.em.getTransaction().rollback();
    }

    @Test
    public void bill_containsItem_true() {
        Customer c = this.customerController.registerCustomer("Josef", "Novák");
        Bill b = this.billController.newBill(c);

        Product p = mock(Product.class);
        when(p.getPrice()).thenReturn(100.0);
        when(p.getId()).thenReturn((long) 10);
        b.getItems().add(new BillConsistsOf(p, b, 10));

        assertEquals(true, b.containsProduct(p));
    }

    @Test
    public void bill_isOverdue() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = format.parse("2009-01-01");
        } catch (Exception e) {}

        Bill b = mock(Bill.class);
        b.setPaidDate(new Date());
        b.setCreatedDate(d);

        boolean result = b.overDueDate();

        assertEquals(false, result);
    }

    @Test
    public void bill_discount() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = format.parse("2009-01-01");
        } catch (Exception e) {}

        Customer c = mock(Customer.class);
        when(c.getRegisteredDate()).thenReturn(d);
        when(c.getBills()).thenReturn(new ArrayList<Bill>());

        Bill b = this.billController.newBill(c);
        Product p = mock(Product.class);
        when(p.getPrice()).thenReturn(100.0);
        b.getItems().add(new BillConsistsOf(p, b, 10));
        b.recount();
        b.setPaidDate(new Date());

        int result = (int)Math.round(this.billController.getPriceAfterDiscount(b));

        assertEquals(900, result);
    }

    @Test
    public void bill_recount() {
        Bill b = new Bill();

        Product p = mock(Product.class);
        when(p.getPrice()).thenReturn(100.0);
        b.getItems().add(new BillConsistsOf(p, b, 10));

        b.recount();

        assertEquals(1000, (int)b.getTotalAmount());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/source.csv", numLinesToSkip = 1)
    public void bill_pairwise(String firstName, String lastName, String registeredDate, String isFirstBill, String price, String createdDate, String paidDate, String expected) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date registered = null;
        Date created = null;
        Date paid = null;
        try {
            registered = format.parse(registeredDate);
            created = format.parse(createdDate);
            paid = format.parse(paidDate);
        } catch (Exception e) {}

        Customer c = this.customerController.registerCustomer(firstName, lastName);
        c.setRegisteredDate(registered);

        boolean firstBill = Boolean.parseBoolean(isFirstBill);
        if (!firstBill) {
            Bill b1 = this.billController.newBill(c);
            this.billController.payBill(b1);
        }

        Bill b2 = this.billController.newBill(c);
        Product p = mock(Product.class);
        when(p.getPrice()).thenReturn(Double.parseDouble(price));
        b2.getItems().add(new BillConsistsOf(p, b2, 1));
        b2.recount();
        b2.setCreatedDate(created);
        b2.setPaidDate(paid);
        b2.setCustomer(c);

        assertEquals(Math.round(Integer.parseInt(expected)), Math.round(this.billController.getPriceAfterDiscount(b2)));
    }
}