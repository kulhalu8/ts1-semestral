package cvut.fel.dbs.dao;

import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.Product;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.SimpleDateFormat;

/**
 * Singleton that holds helper classes
 */
public class DAOHolder {
    private static DAOHolder instance = new DAOHolder( );
    private GenericDao<Product> productDao;
    private GenericDao<Bill> billDao;
    private BillConsistsOfDao billConsistsOfDao;
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private EntityManager em;

    private DAOHolder() {
        init();
    }

    private void init() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");;
        em = emf.createEntityManager();

        productDao = new GenericDao<Product>(Product.class, em);
        billDao = new GenericDao<Bill>(Bill.class, em);
        //billConsistsOfDao = new GenericDao<BillConsistsOf>(BillConsistsOf.class, em);
        billConsistsOfDao = new BillConsistsOfDao(em);
    }

    /**
     * @return Current instance of the singleton
     */
    public static DAOHolder getInstance( ) {
        return instance;
    }

    public GenericDao<Product> getProductDao() {
        return productDao;
    }

    public GenericDao<Bill> getBillDao() {
        return billDao;
    }

    public BillConsistsOfDao getBillConsistsOfDao() {
        return billConsistsOfDao;
    }

    public SimpleDateFormat getDateFormatter() {
        return this.formatter;
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
