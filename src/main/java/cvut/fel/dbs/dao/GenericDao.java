package cvut.fel.dbs.dao;

import javax.persistence.EntityManager;
import java.util.List;


/**
 * Generic DAO for entities without specific needs
 * @param <T> Entity type
 */
public class GenericDao<T> {

    private EntityManager em;

    private Class<T> classType;

    /**
     *
     * @param type Entity type
     * @param em Entity manager unit
     */
    public GenericDao(Class<T> type, EntityManager em) {
        this.classType = type;
        this.em = em;
    }

    /**
     * Find all entities of this type
     * @return List of entities
     */
    public List<T> findAll(){
        return em.createNamedQuery(this.classType.getSimpleName() + ".findAll").getResultList();
    }

    /**
     * Create a new entity and store it
     * @param o Entity to store
     */
    public void create(T o){
        em.persist(o);
    }

    /**
     * Find an entity
     * @param id ID of the entity
     * @return Entity, if found
     */
    public T find(Long id){
        return em.find(this.classType, id);
    }

    /**
     * Update entity
     * @param o Entity to update
     * @return Updated entity
     */
    public T merge(T o){

        //this.em.persist(o);
        return em.merge(o);
    }

    /**
     * Delete entity
     * @param o Entity to delete
     */
    public void delete(T o){
        em.remove(o);
        em.flush();
    }
}
