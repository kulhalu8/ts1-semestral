package cvut.fel.dbs.dao;

import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillConsistsOf;
import cvut.fel.dbs.model.Product;

import javax.persistence.*;
import java.util.List;

public class BillConsistsOfDao {
    private EntityManager em;

    public BillConsistsOfDao(EntityManager em) {
        this.em = em;
    }

    public List<BillConsistsOf> findAllFor(Bill b){
        Query q = em.createQuery("SELECT c FROM BillConsistsOf c WHERE c.bill = :bid");
        q.setParameter("bid", b);
        return q.getResultList();
    }

    public List<BillConsistsOf> findAllFor(Product p){
        Query q = em.createQuery("SELECT c FROM BillConsistsOf c WHERE c.product = :pid");
        q.setParameter("pid", p);
        return q.getResultList();
    }

    public void create(BillConsistsOf o){
        em.persist(o);
    }

    public BillConsistsOf merge(BillConsistsOf o){
        return em.merge(o);
    }

    public void delete(BillConsistsOf o){
        em.remove(o);
        em.flush();
    }
}
