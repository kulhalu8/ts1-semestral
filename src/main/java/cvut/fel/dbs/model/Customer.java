package cvut.fel.dbs.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries(@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c"))
public class Customer {
    @Id
    @GeneratedValue
    private int id;

    private String firstName;

    private String lastName;

    @OneToMany
    private List<Bill> bills;

    @Temporal(TemporalType.DATE)
    private Date registeredDate;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }
}
