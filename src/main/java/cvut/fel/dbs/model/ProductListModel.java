package cvut.fel.dbs.model;

import cvut.fel.dbs.dao.DAOHolder;
import cvut.fel.dbs.view.ProductForm;

import javax.persistence.EntityTransaction;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ProductListModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAMES = new String[] {"Name", "Category", "Price", "Introduced date", "Edit", "Delete"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {String.class, String.class, String.class, String.class, JButton.class};
    private List<Product> products;


    public ProductListModel() {
        super();
        this.refresh();
    }

    public void refresh() {
        this.products = DAOHolder.getInstance().getProductDao().findAll();
        this.fireTableDataChanged();
    }

    @Override public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override public int getRowCount() {
        return this.products.size();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override public Object getValueAt(final int rowIndex, final int columnIndex) {
        Product p = this.products.get(rowIndex);
        ProductListModel listModel = this;

        switch (columnIndex) {
            case 0: return p.getName();
            case 1: return p.getCategory();
            case 2: return p.getPrice();
            case 3: return DAOHolder.getInstance().getDateFormatter().format(p.getIntroducedDate());
            case 4:
                final JButton button = new JButton("Edit product");
                button.addActionListener(e -> {
                        ProductForm pf = new ProductForm(p, listModel);
                        pf.showWindow();
                });
                return button;
            case 5:
                final JButton button2 = new JButton("Delete product");
                button2.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        EntityTransaction trans = DAOHolder.getInstance().getEntityManager().getTransaction();
                        trans.begin();
                        for (BillConsistsOf p : DAOHolder.getInstance().getBillConsistsOfDao().findAllFor(p)) {
                            DAOHolder.getInstance().getBillConsistsOfDao().delete(p);
                            p.getBill().getItems().remove(p);
                            p.getBill().recount();
                        }
                        trans.commit();

                        trans = DAOHolder.getInstance().getEntityManager().getTransaction();
                        trans.begin();
                        try {
                            DAOHolder.getInstance().getProductDao().delete(p);
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                            trans.rollback();
                        }
                        trans.commit();
                        listModel.refresh();
                    }
                });
                return button2;
            default: return "Error";
        }
    }
}
