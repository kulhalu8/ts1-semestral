package cvut.fel.dbs.model;


import java.io.Serializable;
import java.util.Objects;

public class BillConsistsOfId implements Serializable {
    private long product;
    private long bill;

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public long getBill() {
        return bill;
    }

    public void setBill(long bill) {
        this.bill = bill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BillConsistsOfId that = (BillConsistsOfId) o;
        return product == that.product &&
                bill == that.bill;
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, bill);
    }
}
