package cvut.fel.dbs.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries(@NamedQuery(name="Bill.findAll", query="SELECT c FROM Bill c"))
public class Bill {
    @Id
    @GeneratedValue
    private long id;

    @OneToMany (mappedBy = "bill")
    private List<BillConsistsOf> items;

    @ManyToOne
    private Customer customer;

    private double totalAmount;

    @Temporal(TemporalType.DATE)
    private Date createdDate;

    @Temporal(TemporalType.DATE)
    private Date paidDate;

    private boolean isOpen;

    public Bill() {
        this.items = new ArrayList<BillConsistsOf>();
    }

    public long getId() {
        return id;
    }

    public List<BillConsistsOf> getItems() {
        return items;
    }

    public void setItems(List<BillConsistsOf> items) {
        this.items = items;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public boolean containsProduct(Product p) {
        for (BillConsistsOf product : this.items)
            if (product.getProduct().getId() == p.getId())
                return true;
        return false;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public boolean overDueDate() {

        return this.paidDate.compareTo(createdDate) == 0;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void recount() {
        double total = 0;
        for (BillConsistsOf product : this.items)
            total += product.getProduct().getPrice() * product.getAmount();

        this.totalAmount = total;
    }

    public float getPercentOff() {
        float n = 0.0f;

        if (this.customer.getBills().size() == 1) {
            n += 0.05f;
        }
        long years = ChronoUnit.YEARS.between(
                this.customer.getRegisteredDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                LocalDate.now()
        );

        int yearCount = Math.min((int) years / 5, 3);
        n += yearCount * 0.05f;

        return n;
    }

}
