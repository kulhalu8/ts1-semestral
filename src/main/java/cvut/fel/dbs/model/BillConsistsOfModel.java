package cvut.fel.dbs.model;

import cvut.fel.dbs.dao.DAOHolder;

import javax.persistence.EntityTransaction;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BillConsistsOfModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAMES = new String[] {"Product name", "Price", "Amount", "Delete"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {String.class, String.class, String.class, JButton.class};
    private List<BillConsistsOf> products;
    private Bill b;

    public BillConsistsOfModel(Bill b) {
        super();
        this.b = b;
        this.refresh();
    }

    public void refresh() {
        this.products = DAOHolder.getInstance().getBillConsistsOfDao().findAllFor(b);
        this.fireTableDataChanged();
    }

    @Override public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override public int getRowCount() {
        return this.products.size();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override public Object getValueAt(final int rowIndex, final int columnIndex) {
        BillConsistsOf b = this.products.get(rowIndex);
        BillConsistsOfModel listModel = this;

        switch (columnIndex) {
            case 0: return String.valueOf(b.getProduct().getName());
            case 1: return String.valueOf(b.getProduct().getPrice());
            case 2: return String.valueOf(b.getAmount());
            case 3:
                final JButton button = new JButton("Delete product");
                button.addActionListener(e -> {
                    //get new transaction and start it
                    EntityTransaction trans = DAOHolder.getInstance().getEntityManager().getTransaction();
                    trans.begin();

                    //update bill
                    b.getBill().getItems().remove(b);
                    b.getBill().recount();
                    DAOHolder.getInstance().getBillDao().merge(b.getBill());

                    //delete BillConsistsOf object
                    DAOHolder.getInstance().getBillConsistsOfDao().delete(b);
                    trans.commit();

                    listModel.refresh();
                });
                return button;
            default: return "Error";
        }
    }
}
