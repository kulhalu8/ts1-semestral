package cvut.fel.dbs.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries(@NamedQuery(name="Product.findAll", query="SELECT c FROM Product c"))
public class Product {
    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name;

    @Temporal(TemporalType.DATE)
    private Date introducedDate;

    private double price;

    private String category;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getIntroducedDate() {
        return introducedDate;
    }

    public void setIntroducedDate(Date introducedDate) {
        this.introducedDate = introducedDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return name;
    }
}
