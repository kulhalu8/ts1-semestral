package cvut.fel.dbs.model;

import cvut.fel.dbs.dao.DAOHolder;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BillListModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAMES = new String[] {"Bill ID", "Customer name", "Price"};
    private static final Class<?>[] COLUMN_TYPES = new Class<?>[] {String.class, String.class, String.class};
    private List<Bill> bills;


    public BillListModel() {
        super();
        this.refresh();
    }

    public void refresh() {
        this.bills = DAOHolder.getInstance().getBillDao().findAll();
        this.fireTableDataChanged();
    }

    @Override public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override public int getRowCount() {
        return this.bills.size();
    }

    @Override public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override public Object getValueAt(final int rowIndex, final int columnIndex) {
        Bill b = this.bills.get(rowIndex);
        BillListModel listModel = this;

        switch (columnIndex) {
            case 0: return String.valueOf(b.getId());
            case 1: return b.getCustomer().getFirstName() + " " + b.getCustomer().getLastName();
            case 2: return String.valueOf(b.getTotalAmount());
            default: return "Error";
        }
    }
}
