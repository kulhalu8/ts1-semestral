package cvut.fel.dbs.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "BillConsistsOf")
@IdClass(BillConsistsOfId.class)
public class BillConsistsOf implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "productId", referencedColumnName = "id")
    private Product product;

    @Id
    @ManyToOne
    @JoinColumn(name = "billId", referencedColumnName = "id")
    private Bill bill;

    @Column(name = "amount")
    private int amount;

    public BillConsistsOf() {

    }

    public BillConsistsOf(Product p, Bill b, int amount) {
        this.amount = amount;
        this.bill = b;
        this.product = p;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
