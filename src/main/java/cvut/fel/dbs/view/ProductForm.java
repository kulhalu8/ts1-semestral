package cvut.fel.dbs.view;

import cvut.fel.dbs.dao.DAOHolder;
import cvut.fel.dbs.model.Product;
import cvut.fel.dbs.model.ProductListModel;

import javax.persistence.EntityTransaction;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Date;

public class ProductForm {
    private JTextField productName;
    private JTextField categoryName;
    private JTextField price;
    private JTextField introductionDate;
    private JButton submitButton;
    private JButton cancelButton;
    private JPanel jpanel1;
    private JFrame frame;

    private ProductListModel pl;
    private Product p;

    public ProductForm(ProductListModel pl) {
        this.frame = new JFrame("Product form");
        frame.setContentPane(this.jpanel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.pl = pl;

        this.p = new Product();
        ProductForm form = this;
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                form.save();
            }
        });
        cancelButton.addActionListener(e -> {closeWindow();});
    }

    public ProductForm(Product p, ProductListModel pl) {
        this.frame = new JFrame("Product form");
        frame.setContentPane(this.jpanel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.p = p;
        this.pl = pl;

        ProductForm form = this;
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                form.saveChanges();
            }
        });

        cancelButton.addActionListener(e -> {closeWindow();});
        this.setInputs();
    }

    public void setInputs() {
        this.productName.setText(p.getName());
        this.categoryName.setText(p.getCategory());
        this.price.setText(String.valueOf(p.getPrice()));
        this.introductionDate.setText(DAOHolder.getInstance().getDateFormatter().format(p.getIntroducedDate()));
    }

    public boolean parse() {
        p.setName(this.productName.getText());
        p.setCategory(this.categoryName.getText());

        //parse price
        try {
            double price = Double.parseDouble(this.price.getText());
            p.setPrice(price);
        } catch (NumberFormatException e) {
            new ErrorDialog("Please enter valid price");
            return false;
        }

        if (p.getPrice() < 0) {
            new ErrorDialog("Please enter valid price");
            return false;
        }

        //parse date
        try {
            Date date = DAOHolder.getInstance().getDateFormatter().parse(this.introductionDate.getText());
            p.setIntroducedDate(date);
        }
        catch (Exception e) {
            new ErrorDialog("Please enter date in YYYY-MM-DD format");
            return false;
        }

        return true;
    }

    public void save() {
        if (this.parse()) {
            try {
                EntityTransaction trans = DAOHolder.getInstance().getEntityManager().getTransaction();
                trans.begin();
                DAOHolder.getInstance().getProductDao().create(p);
                trans.commit();
                this.pl.refresh();
                this.closeWindow();
            } catch (Exception e) {
                e.printStackTrace();
                new ErrorDialog("Product with given name already exists");
            }
        }
    }

    public void saveChanges() {
        if (this.parse()) {
            try {
                EntityTransaction trans = DAOHolder.getInstance().getEntityManager().getTransaction();
                trans.begin();
                DAOHolder.getInstance().getProductDao().merge(p);
                trans.commit();

                this.pl.refresh();
                this.closeWindow();
            } catch (Exception e) {
                e.printStackTrace();
                new ErrorDialog("Product with given name already exists");
            }
        }
    }

    public void showWindow() {
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
