package cvut.fel.dbs.view;

import cvut.fel.dbs.model.Bill;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class MainWindow {
    private JFrame frame;
    private JButton productListButton;
    private JPanel jpanel;
    private JButton billListButton;

    public MainWindow() {
        productListButton.addActionListener(e -> {
            ProductList window = new ProductList();
            window.showWindow();
        });

        billListButton.addActionListener(e -> {
            BillList window = new BillList();
            window.showWindow();
        });
    }

    public void showWindow() {
        this.frame = new JFrame("Main Window");
        frame.setContentPane(this.jpanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
