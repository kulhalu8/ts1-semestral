package cvut.fel.dbs.view;

import cvut.fel.dbs.model.ProductListModel;

import javax.swing.*;
import java.awt.event.WindowEvent;

public class ProductList {
    private JTable productList;
    private JPanel panel1;
    private JButton refreshButton;
    private JButton addProductButton;
    private JFrame frame;

    public ProductList() {
        this.productList.setModel(new ProductListModel());
        this.productList.getColumn("Edit").setCellRenderer(new ButtonRenderer());
        this.productList.getColumn("Delete").setCellRenderer(new ButtonRenderer());
        this.productList.addMouseListener(new ButtonMouseListener(this.productList));

        refreshButton.addActionListener(e -> {
            this.refresh();
        });

        addProductButton.addActionListener(e -> {
            ProductForm pf = new ProductForm((ProductListModel) this.productList.getModel());
            pf.showWindow();
        });
    }

    public void refresh() {
        ((ProductListModel)(this.productList.getModel())).refresh();
    }

    public void showWindow() {
        this.refresh();

        this.frame = new JFrame("Product list");
        frame.setContentPane(this.panel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
