package cvut.fel.dbs.view;

import cvut.fel.dbs.dao.DAOHolder;
import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillConsistsOfModel;

import javax.swing.*;
import java.awt.event.WindowEvent;

public class BillForm {
    private JLabel billId;
    private JLabel createdDate;
    private JLabel paidDate;
    private JLabel totalAmount;
    private JLabel customerName;
    private JTable table1;
    private JPanel panel1;
    private JButton addProductButton;
    private JFrame frame;
    private JScrollPane productsOnBill;
    private Bill b;

    public BillForm(Bill b) {
        this.b = b;
        this.setData();
        BillForm currentForm = this;
        addProductButton.addActionListener(e -> {
                BillFormAddProduct form = new BillFormAddProduct(currentForm.getBill(), currentForm);
                form.showWindow();
        });

        this.table1.setModel(new BillConsistsOfModel(this.b));
        this.table1.getColumn("Delete").setCellRenderer(new ButtonRenderer());
        this.table1.addMouseListener(new ButtonMouseListener(this.table1));
    }

    public Bill getBill() {
        return this.b;
    }

    public void setData() {
        this.b.recount();
        this.billId.setText(String.valueOf(b.getId()));
        this.createdDate.setText(DAOHolder.getInstance().getDateFormatter().format(b.getCreatedDate()));
        this.paidDate.setText(DAOHolder.getInstance().getDateFormatter().format(b.getPaidDate()));
        this.customerName.setText(b.getCustomer().getFirstName() + " " + b.getCustomer().getLastName());
        this.totalAmount.setText(String.valueOf(b.getTotalAmount()));
    }

    public void refresh() {
        ((BillConsistsOfModel)(this.table1.getModel())).refresh();
        this.setData();
    }

    public void showWindow() {
        this.frame = new JFrame("Bill form");
        frame.setContentPane(this.panel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
