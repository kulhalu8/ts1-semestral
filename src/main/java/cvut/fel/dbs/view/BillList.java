package cvut.fel.dbs.view;

import cvut.fel.dbs.dao.DAOHolder;
import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillListModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

public class BillList {
    private JTable billList;
    private JPanel jpanel1;
    private JFrame frame;

    public BillList() {
        this.billList.setModel(new BillListModel());

        this.billList.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int row = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
                    long billId = Long.parseLong(billList.getValueAt(row, 0).toString());

                    Bill b = DAOHolder.getInstance().getBillDao().find(billId);
                    BillForm bf = new BillForm(b);
                    bf.showWindow();
                }
            }
        });
    }

    public void showWindow() {
        this.frame = new JFrame("Bill list");
        frame.setContentPane(this.jpanel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
