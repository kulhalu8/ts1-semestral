package cvut.fel.dbs.view;

import cvut.fel.dbs.dao.DAOHolder;
import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillConsistsOf;
import cvut.fel.dbs.model.Product;

import javax.persistence.EntityTransaction;
import javax.swing.*;
import java.awt.event.*;

public class BillFormAddProduct extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox product;
    private JTextField amount;
    private JPanel jpanel1;
    private JFrame frame;
    private BillForm billForm;
    private Bill bill;

    public BillFormAddProduct(Bill b, BillForm bf) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.billForm = bf;
        this.bill = b;

        DefaultComboBoxModel cbm = new DefaultComboBoxModel();
        for (Product p : DAOHolder.getInstance().getProductDao().findAll()) {
            if (!b.containsProduct(p))
                cbm.addElement(p);
        }
        this.product.setModel(cbm);
    }


    private void onOK() {
        int amount;
        try {
            amount = Integer.parseInt(this.amount.getText());
        }
        catch (NumberFormatException e) {
            new ErrorDialog("Amount must be an integer");
            return;
        }

        BillConsistsOf productOnBill = new BillConsistsOf((Product)this.product.getSelectedItem(), bill, amount);
        bill.getItems().add(productOnBill);
        bill.recount();

        EntityTransaction transaction = DAOHolder.getInstance().getEntityManager().getTransaction();
        transaction.begin();
        DAOHolder.getInstance().getBillConsistsOfDao().create(productOnBill);
        DAOHolder.getInstance().getBillDao().merge(bill);
        transaction.commit();

        billForm.refresh();

        this.closeWindow();
    }

    private void onCancel() {
        // add your code here if necessary
        this.closeWindow();
    }

    public void showWindow() {
        this.frame = new JFrame("Bill form - add new product");
        frame.setContentPane(this.contentPane);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void closeWindow() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
