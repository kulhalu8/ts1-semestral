package cvut.fel.dbs.controller;

import cvut.fel.dbs.dao.GenericDao;
import cvut.fel.dbs.model.Bill;
import cvut.fel.dbs.model.BillConsistsOf;
import cvut.fel.dbs.model.Customer;
import cvut.fel.dbs.model.Product;

import javax.persistence.EntityManager;
import java.util.Date;

public class BillController {

    private GenericDao<Bill> dao;

    public BillController(EntityManager em) {
        this.dao = new GenericDao<Bill>(Bill.class, em);
    }

    public void order(Customer c, Product p, int amount) {
        Bill b = this.newBill(c);

        if (!b.containsProduct(p)) {
            BillConsistsOf bco = new BillConsistsOf();
            bco.setBill(b);
            bco.setProduct(p);
            bco.setAmount(amount);
            b.getItems().add(bco);
        }
    }

    public double getPriceAfterDiscount(Bill b) {
        double amount = b.getTotalAmount() - (double)(b.getTotalAmount() * b.getPercentOff());
        if (b.overDueDate())
            amount += b.getTotalAmount() * 0.05f;
        return amount;
    }

    public Bill newBill(Customer c) {
        for (Bill b : c.getBills()) {
            if (b.isOpen())
                return b;
        }

        Bill b = new Bill();
        b.setCustomer(c);
        b.setCreatedDate(new Date());
        b.setOpen(true);

        c.getBills().add(b);
        this.dao.create(b);

        return b;
    }

    public boolean payBill(Bill b) {
        if(b.isOpen()) {
            b.setOpen(false);
            b.setPaidDate(new Date());
            return true;
        }
        return false;
    }
}
