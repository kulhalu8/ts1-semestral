package cvut.fel.dbs.controller;

import cvut.fel.dbs.dao.GenericDao;
import cvut.fel.dbs.model.Customer;

import javax.persistence.EntityManager;
import java.util.Date;

public class CustomerController {
    private GenericDao<Customer> dao;

    public CustomerController(EntityManager em) {
        this.dao = new GenericDao<Customer>(Customer.class, em);
    }

    public Customer registerCustomer(String firstName, String lastName) {
        Customer c = new Customer();
        c.setFirstName(firstName);
        c.setLastName(lastName);
        c.setRegisteredDate(new Date());

        return dao.merge(c);
    }
}
